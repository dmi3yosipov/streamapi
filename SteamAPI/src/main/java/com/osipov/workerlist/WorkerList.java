package com.osipov.workerlist;

import com.osipov.worker.Worker;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class WorkerList {
    private List<Worker> workerList = new LinkedList<>();

    public List<Worker> getWorkerList() {
        return workerList;
    }

    public WorkerList add(Worker worker) {
        workerList.add(worker);
        return this;
    }

    public void deleteByMaxAge() {
        Worker maxAgeWorker = workerList.stream().max(Comparator.comparing(Worker::getAge)).get();
        workerList = workerList.stream()
                .filter(worker -> !(worker.equals(maxAgeWorker)))
                .collect(Collectors.toList());
    }

    public void deleteBySalaryNotInRange(int from, int to) {
        workerList = workerList.stream()
                .filter(worker -> (worker.getSalary() >= from && worker.getSalary() <= to))
                .collect(Collectors.toList());
    }

    public Integer getTotalSalary() {
        return workerList.stream().mapToInt(Worker::getSalary).sum();
    }

    public Worker getWithIndex(int index) {
        return workerList.stream().skip(index - 1).findFirst().get();
    }

    public List<Worker> getSublist(int from, int count) {
        return workerList.stream()
                .skip(from)
                .limit(count)
                .collect(Collectors.toList());
    }

    public List<Worker> getWithNameContaining(String substr) {
        return workerList.stream()
                .filter(worker -> worker.getName().contains(substr))
                .collect(Collectors.toList());
    }

    public boolean isAllHasInName(String substr) {
        return workerList.stream()
                .allMatch(worker -> worker.getName().contains(substr));
    }

    public void addToName(String substr) {
        workerList.forEach(worker -> worker.setName(worker.getName() + substr));
    }

    public void sortByAgeThenBySalary() {
        workerList = workerList.stream()
                .sorted((worker1, worker2) -> !(worker1.getAge().equals(worker2.getAge()))
                        ? worker1.getAge().compareTo(worker2.getAge())
                        : worker1.getSalary().compareTo(worker2.getSalary()))
                .collect(Collectors.toList());
    }
}
