import com.osipov.worker.Worker;
import com.osipov.workerlist.WorkerList;

public class Main {
    public static void main(String[] args) {
        WorkerList workerList = new WorkerList();
        workerList.add(new Worker("Mike", 20, 1000))
                .add(new Worker("Sam", 36, 3500))
                .add(new Worker("Tom", 25, 1400))
                .add(new Worker("Saimon", 28, 3000))
                .add(new Worker("Alex", 32, 3800));
        workerList.getWorkerList().forEach(System.out::println);
        System.out.println("total salary: " + workerList.getTotalSalary());
        System.out.println("third: " + workerList.getWithIndex(3));
        System.out.println(workerList.getSublist(2, 2));
        System.out.println(workerList.getWithNameContaining("m"));
        System.out.println(workerList.isAllHasInName("a"));
        workerList.addToName("_1");
        workerList.getWorkerList().forEach(System.out::println);
        System.out.println("---------------");
        workerList.sortByAgeThenBySalary();
        workerList.getWorkerList().forEach(System.out::println);
        System.out.println("---------------");
        workerList.deleteByMaxAge();
        workerList.getWorkerList().forEach(System.out::println);
        System.out.println("---------------");
        workerList.deleteBySalaryNotInRange(1400, 3000);
        workerList.getWorkerList().forEach(System.out::println);
    }
}
